from django.urls import path

from .views_api import RegistrationAPIView, LoginUserAPIView, LogoutUserAPIView


urlpatterns = [
    path("register/", RegistrationAPIView.as_view(), name="api_accounts_register"),
    path("login/", LoginUserAPIView.as_view(), name="api_accounts_login"),
    path("logout/", LogoutUserAPIView.as_view(), name="api_accounts_logout"),
]
