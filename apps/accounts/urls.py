from django.contrib.auth import views as auth_views
from django.urls import path

from .forms import LoginForm
from .views import RegistrationView


urlpatterns = [
    path(
        "login/",
        auth_views.LoginView.as_view(form_class=LoginForm, redirect_authenticated_user=True),
        name="accounts_login",
    ),
    path("logout/", auth_views.LogoutView.as_view(), name="accounts_logout"),
    path("register/", RegistrationView.as_view(), name="accounts_register"),
]
