from django.db import models


class NotEmptyCharField(models.CharField):
    empty_strings_allowed = False
