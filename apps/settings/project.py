from .env import env


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env.str("SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env.bool("DEBUG", default=False)

# Database
# https://docs.djangoproject.com/en/4.0/ref/settings/#databases
DATABASES = {
    "default": {
        "ENGINE": env.str("DATABASES_ENGINE"),
        "NAME": env.str("DATABASES_NAME"),
        "USER": env.str("DATABASES_USER"),
        "PASSWORD": env.str("DATABASES_PASSWORD"),
        "HOST": env.str("DATABASES_HOST"),
        "PORT": env.int("DATABASES_PORT"),
    }
}

CSRF_TRUSTED_ORIGINS = env.list("CSRF_TRUSTED_ORIGINS")

ALLOWED_HOSTS = env.list("ALLOWED_HOSTS")

TIME_ZONE = "Europe/Moscow"
LANGUAGE_CODE = "ru-ru"

INTERNAL_IPS = [
    # ...
    "127.0.0.1",
    # ...
]

TEST_RUNNER = env.str("TEST_RUNNER", default="django.test.runner.DiscoverRunner")
