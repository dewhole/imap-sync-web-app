from pathlib import Path

import environ


env = environ.Env(
    # set casting, default value
    DEBUG=(bool, False),
)
env.read_env(Path("/.env").expanduser())
env.read_env(Path.joinpath(Path(__file__).resolve().parent.parent.parent, "../.env"))
