from django_filters.rest_framework import DjangoFilterBackend
from django.contrib.auth import get_user_model
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import ListAPIView
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

from apps.common.utils import PaginationBy50
from .models import Log
from .filter import LogsFilter
from .serializers import LogSerializer

User = get_user_model()


class LogsAPIView(ListAPIView):
    """
    Shows a list of filtered logs from :model:`logs.Logs` :filter:`LogsFilter`.
    """

    serializer_class = LogSerializer
    filterset_class = LogsFilter
    pagination_class = PaginationBy50
    filter_backends = [DjangoFilterBackend]
    permission_classes = [IsAuthenticated]
    authentication_classes = (TokenAuthentication, SessionAuthentication)

    def get_queryset(self):
        queryset = Log.objects.filter(user=self.request.user) | Log.objects.filter(user=None)
        self.filter = self.filterset_class(self.request.GET, queryset=queryset)
        return self.filter.qs.distinct().order_by("-datetime")

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter("page", openapi.IN_QUERY, description="Page number", type=openapi.TYPE_INTEGER),
            openapi.Parameter(
                "page_size", openapi.IN_QUERY, description="Elements number on page", type=openapi.TYPE_INTEGER
            ),
            openapi.Parameter(
                "level",
                openapi.IN_QUERY,
                description="Log level",
                type=openapi.TYPE_STRING,
            ),
            openapi.Parameter(
                "sender",
                openapi.IN_QUERY,
                description="App sender log",
                type=openapi.TYPE_STRING,
            ),
        ]
    )
    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        serializer = self.get_serializer(page, many=True)

        response_data = []
        response_data.extend(serializer.data)
        response_data.append({"filters": {"levels": [], "senders": []}})
        response_data[-1]["filters"]["levels"].extend(LogsFilter().filters["level"].field.choices)
        response_data[-1]["filters"]["senders"].extend(LogsFilter().filters["sender"].field.choices)
        return self.get_paginated_response(response_data)
