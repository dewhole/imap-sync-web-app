from django.urls import path

from .views_api import LogsAPIView


urlpatterns = [
    path("", LogsAPIView.as_view(), name="api_logs_list"),
]
