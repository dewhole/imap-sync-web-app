from django.test import TestCase
from django.contrib.auth import get_user_model

from ..models import Log

User = get_user_model()


class LogViewsTest(TestCase):
    def setUp(self):
        self.user_1 = User.objects.create_user(username="test", password="test")
        self.user_2 = User.objects.create_user(username="test2", password="test2")
        self.log_1 = Log.objects.create(user=self.user_1, sender="test_sender", title="test title", level=50)
        self.log_2 = Log.objects.create(user=self.user_2, sender="test sender", title="test title", level=40)
        self.log_3 = Log.objects.create(user=None, sender="test sender", title="test title", level=40)

    def tearDown(self):
        self.user_1.delete()
        self.user_2.delete()
        self.log_1.delete()
        self.log_2.delete()
        self.log_3.delete()

    def test_logs_view(self):
        self.client.force_login(User.objects.get(username="test"), backend=None)
        response = self.client.get("/logs/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context["object_list"]), 2)
