from django.contrib import admin

from .models import Log


@admin.register(Log)
class LogAdmin(admin.ModelAdmin):
    list_display = ["user", "sender", "title", "level", "datetime"]
    list_filter = ["datetime", "level", "sender", "user"]
    search_fields = ["sender", "title"]
