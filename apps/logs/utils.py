from django.conf import settings
from django.contrib.auth import get_user_model

from .models import Log

User = get_user_model()


def log(sender: str, title: str, level: int, mail_id, user: User = None):
    if settings.LOGGER_BACKEND == "DB":
        Log.objects.create(sender=sender, title=title, level=level, mail_id=mail_id, user=user)
    elif settings.LOGGER_BACKEND == "PRINT":
        print(sender, title, mail_id, level, user)
