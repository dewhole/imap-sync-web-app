from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model

User = get_user_model()


class Log(models.Model):
    """
    Stores a single log entry, related to :model:`account.User`.
    """

    LEVELS = [
        (0, "NOTSET"),
        (10, "DEBUG"),
        (20, "INFO"),
        (30, "WARNING"),
        (40, "ERROR"),
        (50, "CRITICAL"),
    ]

    user = models.ForeignKey(User, related_name=_("User"), on_delete=models.CASCADE, blank=True, null=True)
    mail_id = models.CharField(_("Mail ID"), max_length=128, blank=True, null=True)
    sender = models.CharField(_("Sender"), max_length=128, blank=True, null=True)
    title = models.CharField(_("Title"), max_length=128)
    level = models.PositiveSmallIntegerField(_("Level"), choices=LEVELS, default=0)
    datetime = models.DateTimeField(_("Date and time"), auto_now_add=True)

    class Meta:
        verbose_name = _("Log")
        verbose_name_plural = _("Logs")
        ordering = ["-datetime"]
