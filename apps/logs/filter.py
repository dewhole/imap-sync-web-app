import django
import django_filters

from .models import Log


class LogsFilter(django_filters.FilterSet):
    level = django_filters.ChoiceFilter(field_name="level", choices=Log.LEVELS)

    try:
        sender = django_filters.ChoiceFilter(
            choices=[
                [sender, sender]
                for sender in Log.objects.values_list("sender", flat=True).distinct().order_by("sender")
            ],
        )
    except django.db.utils.ProgrammingError as exc:
        # Log table is referenced here before it's created with manage.py migrate
        if 'relation "logs_log" does not exist' not in str(exc):
            raise exc

    class Meta:
        model = Log
        fields = ["level", "sender"]
