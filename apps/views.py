from django.shortcuts import render
from django.core.handlers.wsgi import WSGIRequest


def main(request: WSGIRequest) -> render:
    """Returns the main page"""
    return render(request, "main.html")
