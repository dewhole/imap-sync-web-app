import subprocess

from apps.logs.utils import log
from .models import Mail


def sync_email(mail_id, source_server, source_user, source_password, source_ssl, target_server, target_user, target_password, target_ssl):
    command = [
        "imapsync",
        "--no-log",
        "--host1", source_server,
        "--user1", source_user,
        "--password1", source_password,
        "--host2", target_server,
        "--user2", target_user,
        "--password2", target_password,
    ]

    if source_ssl:
        command.append("--ssl1")

    if target_ssl:
        command.append("--ssl2")

    with open(f"./logs/{mail_id}.txt", "w") as f:
        process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        while True:
            output = process.stdout.readline()
            if not output and process.poll() is not None:
                break
            if output:
                f.write(output.decode("utf-8"))
                f.flush()

        stderr = process.communicate()[1]

    if process.returncode != 0:
        mail = Mail.objects.get(id=mail_id) 
        mail.status = "Error"
        mail.save()
        log("sync_email", f"Task failed with error: {stderr}", mail_id, 40)
    else:
        mail = Mail.objects.get(id=mail_id) 
        mail.status = "Done"
        mail.save()
        log("sync_email", f"Task completed successfully", mail_id, 20)