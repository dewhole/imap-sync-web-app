from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.views import generic
from django.urls import reverse
from django.db.models import Q

from .models import Mail
from .forms import MailForm, MailFileForm
from .tasks import sync_email_task
from ..settings.django import BASE_DIR


class MailMassSyncView(LoginRequiredMixin, generic.View):
    model = Mail

    def get(self, request, *args, **kwargs):
        mails = Mail.objects.filter(Q(status="Not sync") | Q(status="Error"))
        Mail.objects.filter(status="Not Sync").update(status="Sync started")
        for mail in mails:
            sync_email_task.delay(mail.id, mail.source_server, mail.source_user, mail.source_password, mail.source_ssl, mail.target_server, mail.target_user, mail.target_password, mail.target_ssl)
        return redirect("imapsync_mail_list")
        

class MailCreateFromFileView(LoginRequiredMixin, generic.CreateView):
    form_class = MailFileForm
    model = Mail
    template_name_suffix = "_form_file"


    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            data = form.cleaned_data['file'].read().decode('utf-8')
            mails = []
            for line in data.split("\n"):
                data_line = line.split(" ")
                if len(data_line) > 2:
                    mail_object = Mail(
                        user=request.user, 
                        source_server=form.cleaned_data['source_server'],
                        source_user=data_line[0], 
                        source_password=data_line[1],
                        source_ssl=form.cleaned_data['source_ssl'], 
                        target_server=form.cleaned_data['target_server'],
                        target_user=data_line[2], 
                        target_password=data_line[3],
                        target_ssl=form.cleaned_data['source_ssl'],
                        status="Not sync"
                    )
                    mails.append(mail_object)
                Mail.objects.bulk_create(mails, ignore_conflicts=True)
            return redirect("imapsync_mail_list")
        else:
            print(form.errors.as_text())
            return self.form_invalid(form)


class MailCreateView(LoginRequiredMixin, generic.CreateView):
    form_class = MailForm
    model = Mail

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            new_mail = form.save(commit=False)
            new_mail.user = request.user
            new_mail.status = "Not sync"
            new_mail.save()
            return redirect("imapsync_mail_list")
        else:
            return self.form_invalid(form)


class MailListView(LoginRequiredMixin, generic.ListView):
    model = Mail
    paginate_by = 20

    def get_queryset(self):
        return Mail.objects.filter(user=self.request.user).order_by("-id")


class MailDetailView(LoginRequiredMixin, generic.DetailView):
    model = Mail

    def dispatch(self, request, *args, **kwargs):
        if self.get_object().user != request.user:
            return redirect("imapsync_mail_list")
        else:
            return self.get(request, *args, **kwargs)


class MailUpdateView(LoginRequiredMixin, generic.UpdateView):
    form_class = MailForm
    model = Mail

    def dispatch(self, request, *args, **kwargs):
        if self.get_object().user != self.request.user:
            return redirect("imapsync_mail_list")
        return super(MailUpdateView, self).dispatch(request, *args, **kwargs)

    def post(self, request, **kwargs):
        new_mail = self.get_object()
        new_mail.save()
        return super(MailUpdateView, self).post(request, **kwargs)

    def get_success_url(self):
        return reverse("imapsync_mail_list")


class MailDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = Mail

    def dispatch(self, request, *args, **kwargs):
        return super(MailDeleteView, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse("imapsync_mail_list")


class MailStartSyncView(LoginRequiredMixin, generic.DetailView):
    model = Mail

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj.user == request.user:
            obj.status = "Sync started"
            obj.save()
            sync_email_task.delay(obj.id, obj.source_server, obj.source_user, obj.source_password, obj.source_ssl, obj.target_server, obj.target_user, obj.target_password, obj.target_ssl)
            return redirect("imapsync_mail_list")
        else:
            return redirect("imapsync_mail_list")
    

class MailViewLogView(LoginRequiredMixin, generic.DetailView):
    template_name_suffix = "_log_detail"
    model = Mail

    def get_object(self):
        obj = super().get_object()
        with open(f"{BASE_DIR}/logs/{obj.id}.txt", 'r') as f:
            obj.content = f.read()
        return obj