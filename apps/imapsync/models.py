from django.utils.translation import gettext_lazy as _
from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class Mail(models.Model):
    user = models.ForeignKey(User, verbose_name=_("User"), on_delete=models.CASCADE)

    source_server = models.CharField(_("Source server"), max_length=40)
    source_user = models.CharField(_("Source user"), max_length=40)
    source_password = models.CharField(_("Source password"), max_length=40)
    source_ssl = models.BooleanField(_("Source ssl"), blank=True, null=True)

    target_server = models.CharField(_("Target server"), max_length=40)
    target_user = models.CharField(_("Target user"), max_length=40)
    target_password = models.CharField(_("Target password"), max_length=40)
    target_ssl = models.BooleanField(_("Target ssl"), blank=True, null=True)

    status = models.CharField(_("Status"), max_length=40)
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)
    sync_at = models.DateTimeField(_("Sync at"), blank=True, null=True, auto_now=True)
    is_active = models.BooleanField(_("Is active"), blank=True, null=True)

    class Meta:
        verbose_name = _("Mail")
        verbose_name_plural = _("Mails")