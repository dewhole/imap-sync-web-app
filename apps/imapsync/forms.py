from django import forms

from apps.common.forms import BootstrapFormMixin
from .models import Mail


class MailForm(BootstrapFormMixin, forms.ModelForm):
    
    class Meta:
        model = Mail
        fields = ["source_server", "source_user", "source_password", "source_ssl", "target_server", "target_user", "target_password", "target_ssl"]


class MailFileForm(BootstrapFormMixin, forms.ModelForm):
    file = forms.FileField(label='Upload File', required=False)

    class Meta:
        model = Mail
        fields = ["source_server", "source_ssl", "target_server", "target_ssl" , "file",]