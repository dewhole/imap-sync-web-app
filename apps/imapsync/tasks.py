from celery import shared_task

from .utils import sync_email


@shared_task(max_concurrency=10)
def sync_email_task(mail_id, source_server, source_user, source_password, source_ssl, target_server, target_user, target_password, target_ssl):
    sync_email(mail_id, source_server, source_user, source_password, source_ssl, target_server, target_user, target_password, target_ssl)
    