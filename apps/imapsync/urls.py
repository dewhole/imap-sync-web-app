from django.urls import path

from .views import (
    MailListView,
    MailCreateView,
    MailUpdateView,
    MailDetailView,
    MailDeleteView,
    MailStartSyncView,
    MailCreateFromFileView,
    MailMassSyncView,
    MailViewLogView,
)


urlpatterns = [
    path("mail/", MailListView.as_view(), name="imapsync_mail_list"),
    path("mail/add/", MailCreateView.as_view(), name="imapsync_mail_add"),
    path("mail/add_file/", MailCreateFromFileView.as_view(), name="imapsync_mail_add_file"),
    path("mail/edit/<int:pk>/", MailUpdateView.as_view(), name="imapsync_mail_edit"),
    path("mail/view/<int:pk>/", MailDetailView.as_view(), name="imapsync_mail_view"),
    path("mail/delete/<int:pk>/", MailDeleteView.as_view(), name="imapsync_mail_delete"),
    path("mail/start_sync.<int:pk>/", MailStartSyncView.as_view(), name="imapsync_mail_start_sync"),
    path("mail/view_log.<int:pk>/", MailViewLogView.as_view(), name="imapsync_mail_view_log"),
    path("mail_mass_sync/", MailMassSyncView.as_view(), name="imapsync_mail_mass_sync"),

]
