from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from gspread.exceptions import APIError

from apps.google_ads.models import Sheet
from apps.common.models import Currency
from apps.logs.utils import log
from apps.google_ads.utils import access_to_google_sheet, save_keyword_data, clear_google_table

User = get_user_model()


class Command(BaseCommand):
    help = "getting data from google sheet for each table and writing to database"

    def handle(self, *args, **options):
        for sheet in Sheet.objects.filter(is_active=True):
            # Reversing and google spreadsheet and getting all rows
            try:
                keywords_data_list, worksheet = access_to_google_sheet(sheet)
            except APIError or ValidationError as e:
                log(
                    "google_ads.management.commands.google_sheet_get_data",
                    f"access to googlesheet (id: {sheet.id}) has problem: {e}",
                    40,
                    sheet.user,
                )
                continue
            except ValidationError as e:
                log(
                    "google_ads.management.commands.google_sheet_get_data",
                    f"access to googlesheet (id: {sheet.id}) has problem: {e}",
                    40,
                    sheet.user,
                )
                continue
            except ValueError as e:
                log(
                    "google_ads.management.commands.google_sheet_get_data",
                    f"access to googlesheet (id: {sheet.id}) has problem: {e}",
                    40,
                    sheet.user,
                )
                continue
            except ConnectionError as e:
                log(
                    "google_ads.management.commands.google_sheet_get_data",
                    f"access to googlesheet (id: {sheet.id}) has problem: {e}",
                    40,
                    sheet.user,
                )
                continue
            # Getting a list of currencies from the database
            currency_list = (
                Currency.objects.all().order_by("-date").distinct("date", "currency").values("date", "currency", "rate")
            )
            # Processing rows from Google spreadsheets and entering into the database
            for keyword_data in keywords_data_list:
                # Receiving and processing data for each table cell
                try:
                    save_keyword_data(keyword_data, currency_list, sheet)
                except LookupError as e:
                    log(
                        "google_ads.management.commands.google_sheet_get_data",
                        f"unable to clear google spreadsheet (id: {sheet.id}) cells: {e}",
                        40,
                        sheet.user,
                    )
                    continue
                except ValueError as e:
                    log(
                        "google_ads.management.commands.google_sheet_get_data",
                        f"unable to clear google spreadsheet (id: {sheet.id}) cells: {e}",
                        40,
                        sheet.user,
                    )
                    continue

            try:
                clear_google_table(worksheet)
            except AttributeError as e:
                log(
                    "google_ads.management.commands.google_sheet_get_data",
                    f"unable to clear google spreadsheet (id: {sheet.id}) cells: {e}",
                    40,
                    sheet.user,
                )
                continue
            except ConnectionError as e:
                log(
                    "google_ads.management.commands.google_sheet_get_data",
                    f"unable to clear google spreadsheet (id: {sheet.id}) cells: {e}",
                    40,
                    sheet.user,
                )
                continue
