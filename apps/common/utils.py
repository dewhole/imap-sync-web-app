from rest_framework.pagination import PageNumberPagination


class PaginationBy10(PageNumberPagination):
    page_size = 10
    page_size_query_param = "page_size"
    max_page_size = 1000


class PaginationBy50(PageNumberPagination):
    page_size = 50
    page_size_query_param = "page_size"
    max_page_size = 1000
