# syntax=docker/dockerfile:1
FROM python:3.11-slim-buster
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY requirements.txt requirements.txt
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN apt update
RUN apt install gettext ssh -y
COPY . .
RUN cp .env /
RUN ["chmod", "+x", "/code/entrypoint.sh"]
ENTRYPOINT ["/code/entrypoint.sh"]
